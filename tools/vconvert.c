/*
 * vconvert - A tool for converting vcards, vevents, vtodos and vnotes
 * Copyright (C) 2004-2005  Armin Bauer <armin.bauer@opensync.org>
 * Copyright (C) 2007  Christopher Stender <cstender@suse.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 *
 * Author: Chris Toshok (toshok@ximian.com)
 * Author: Armin Bauer (armin.bauer@opensync.org)
 * Author: Christopher Stender (cstender@suse.de)
 *
 */

#include <opensync/opensync.h>
#include <opensync/opensync-format.h>
#include <opensync/opensync-data.h>

#include <stdio.h>
#include <string.h>

typedef enum conv_detection {
	TARGET_AUTO = 0,
	TARGET_VCARD_21 = 1,
	TARGET_VCARD_30 = 2,
	TARGET_VEVENT_10 = 3,
	TARGET_VEVENT_20 = 4,
	TARGET_VNOTE_11 = 5,
	TARGET_VJOURNAL = 6,
	TARGET_VTODO_10 = 7,
	TARGET_VTODO_20 = 8,
	TARGET_XMLFORMAT = 9
} conv_detection;

static void usage (char *name, int ecode)
{
	fprintf (stderr, "Usage: %s <file> <switches>\n\n", name);
	fprintf (stderr, "Switches:\n");
	fprintf (stderr, "--help\t\tShow this help message\n");
	fprintf (stderr, "--out <file>\tStore the output in this file (No output to stdout)\n");
	fprintf (stderr, "--format-config <config>\t Apply conversion config (e.g. VCARD_EXTENSION=KDE)\n");
	fprintf (stderr, "--to-vcard21\tConvert to vcard 2.1\n");
	fprintf (stderr, "--to-vcard30\tConvert to vcard 3.0\n");
	fprintf (stderr, "--to-vevent10\tConvert to vevent 1.0\n");
	fprintf (stderr, "--to-vevent20\tConvert to vevent 2.0\n");
	fprintf (stderr, "--to-vnote11\tConvert to vnote 1.1\n");
	fprintf (stderr, "--to-vjournal\tConvert to vjournal\n");
	fprintf (stderr, "--to-vtodo10\tConvert to vtodo 1.0\n");
	fprintf (stderr, "--to-vtodo20\tConvert to vtodo 2.0\n");
	fprintf (stderr, "--to-xmlformat\tConvert to xmlformat\n");
	exit (ecode);
}

OSyncObjFormat *conv_run_detection(OSyncFormatEnv *env, const char *sfname, conv_detection type)
{
	OSyncObjFormat *targetformat = NULL;

	if (!strcmp(sfname, "vcard21")) {
		switch (type) {
		case TARGET_AUTO:
		case TARGET_VCARD_30:
			targetformat = osync_format_env_find_objformat(env, "vcard30");
			break;
		case TARGET_XMLFORMAT:
			targetformat = osync_format_env_find_objformat(env, "xmlformat-contact");
			break;
		default:
			fprintf(stderr, "Unable to convert vcard21 into this format. Supported formats: xmlformat, vcard30\n");
		}
	} else if (!strcmp(sfname, "vcard30")) {
		switch (type) {
		case TARGET_AUTO:
		case TARGET_VCARD_21:
			targetformat = osync_format_env_find_objformat(env, "vcard21");
			break;
		case TARGET_XMLFORMAT:
			targetformat = osync_format_env_find_objformat(env, "xmlformat-contact");
			break;
		default:
			fprintf(stderr, "Unable to convert vcard30 into this format. Supported formats: xmlformat, vcard21\n");
		}
	} else if (!strcmp(sfname, "vevent10")) {
		switch (type) {
                case TARGET_AUTO:
                case TARGET_VEVENT_20:
                        targetformat = osync_format_env_find_objformat(env, "vevent20");
                        break;
                case TARGET_XMLFORMAT:
                        targetformat = osync_format_env_find_objformat(env, "xmlformat-event");
                        break;
                default:
                        fprintf(stderr, "Unable to convert vevent10 into this format. Supported formats: xmlformat, vevent20\n");
                }
        } else if (!strcmp(sfname, "vevent20")) {
		switch (type) {
		case TARGET_AUTO:
		case TARGET_VEVENT_10:
			targetformat = osync_format_env_find_objformat(env, "vevent10");
			break;
		case TARGET_XMLFORMAT:
			targetformat = osync_format_env_find_objformat(env, "xmlformat-event");
			break;
		default:
			fprintf(stderr, "Unable to convert vevent20 into this format. Supported formats: xmlformat, vevent10\n");
		}
	} else if (!strcmp(sfname, "vnote11")) {
		switch (type) {
		case TARGET_AUTO:
		case TARGET_VJOURNAL:
			targetformat = osync_format_env_find_objformat(env, "vjournal");
			break;
		case TARGET_XMLFORMAT:
			targetformat = osync_format_env_find_objformat(env, "xmlformat-note");
			break;
		default:
			fprintf(stderr, "Unable to convert vnote11 into this format. Supported formats: xmlformat, vjournal\n");
		}
	} else if (!strcmp(sfname, "vjournal")) {
		switch (type) {
		case TARGET_AUTO:
		case TARGET_VNOTE_11:
			targetformat = osync_format_env_find_objformat(env, "vnote11");
			break;
		case TARGET_XMLFORMAT:
			targetformat = osync_format_env_find_objformat(env, "xmlformat-note");
			break;
		default:
			fprintf(stderr, "Unable to convert vjournal into this format. Supported formats: xmlformat, vnote11\n");
		}
	} else if (!strcmp(sfname, "vtodo10")) {
		switch (type) {
		case TARGET_AUTO:
		case TARGET_VTODO_20:
			targetformat = osync_format_env_find_objformat(env, "vtodo20");
			break;
		case TARGET_XMLFORMAT:
			targetformat = osync_format_env_find_objformat(env, "xmlformat-todo");
			break;
		default:
			fprintf(stderr, "Unable to convert vtodo10 into this format. Supported formats: xmlformat, vtodo20\n");
		}
	} else if (!strcmp(sfname, "vtodo20")) {
		switch (type) {
		case TARGET_AUTO:
		case TARGET_VTODO_10:
			targetformat = osync_format_env_find_objformat(env, "vtodo10");
			break;
		case TARGET_XMLFORMAT:
			targetformat = osync_format_env_find_objformat(env, "xmlformat-todo");
			break;
		default:
			fprintf(stderr, "Unable to convert vtodo20 into this format. Supported formats: xmlformat, vtodo10\n");
		}
	} else if (!strcmp(sfname, "xmlformat-contact-doc")) {
		switch (type) {
		case TARGET_VCARD_21:
			targetformat = osync_format_env_find_objformat(env, "vcard21");
			break;
		case TARGET_AUTO:
		case TARGET_VCARD_30:
			targetformat = osync_format_env_find_objformat(env, "vcard30");
			break;
		default:
			fprintf(stderr, "Unable to convert xmlformat-contact-doc into this format. Supported formats: vcard21, vcard30\n");
		}
	} else if (!strcmp(sfname, "xmlformat-event-doc")) {
		switch (type) {
		case TARGET_VEVENT_10:
			targetformat = osync_format_env_find_objformat(env, "vevent10");
			break;
		case TARGET_AUTO:
		case TARGET_VEVENT_20:
			targetformat = osync_format_env_find_objformat(env, "vevent20");
			break;
		default:
			fprintf(stderr, "Unable to convert xmlformat-event-doc into this format. Supported formats: vevent10, vevent20\n");
		}
	} else if (!strcmp(sfname, "xmlformat-note-doc")) {
		switch (type) {
		case TARGET_AUTO:
		case TARGET_VNOTE_11:
			targetformat = osync_format_env_find_objformat(env, "vnote11");
			break;
		default:
			fprintf(stderr, "Unable to convert xmlformat-note-doc into this format. Supported formats: vnote11\n");
		}
	} else if (!strcmp(sfname, "xmlformat-journal-doc")) {
		switch (type) {
		case TARGET_AUTO:
		case TARGET_VJOURNAL:
			targetformat = osync_format_env_find_objformat(env, "vjournal");
			break;
		default:
			fprintf(stderr, "Unable to convert xmlformat-journal-doc into this format. Supported formats: vjournal\n");
		}
	} else if (!strcmp(sfname, "xmlformat-todo-doc")) {
		switch (type) {
		case TARGET_VTODO_10:
			targetformat = osync_format_env_find_objformat(env, "vtodo10");
			break;
		case TARGET_AUTO:
		case TARGET_VTODO_20:
			targetformat = osync_format_env_find_objformat(env, "vtodo20");
			break;
		default:
			fprintf(stderr, "Unable to convert xmlformat-todo-doc into this format. Supported formats: vtodo10, vtodo20\n");
		}
	} else {
		fprintf(stderr, "Cannot convert format \"%s\". Unable to find a converter\n", sfname);
	}

	return targetformat;
}

int main (int argc, char *argv[])
{
	OSyncData *data = NULL;
	OSyncError *error = NULL;
	OSyncFormatEnv *format_env = NULL;
	OSyncObjFormat *sourceformat = NULL;
	OSyncObjFormat *targetformat = NULL;
	conv_detection type = TARGET_AUTO;
	char *output = NULL;
	char *format_config = NULL;
	char *filename = NULL;
	char *buffer = NULL;
	char *print = NULL;
	unsigned int size = 0;

	if (argc < 2)
		usage (argv[0], 1);

	if (!strcmp (argv[1], "--help"))
		usage (argv[0], 0);

	filename = argv[1];

	int i;
	for (i = 2; i < argc; i++) {
		char *arg = argv[i];
		if (!strcmp (arg, "--help")) {
			usage (argv[0], 0);
		} else if (!strcmp (arg, "--to-vcard21")) {
			type = TARGET_VCARD_21;
		} else if (!strcmp (arg, "--to-vcard30")) {
			type = TARGET_VCARD_30;
		} else if (!strcmp (arg, "--to-vevent10")) {
			type = TARGET_VEVENT_10;
		} else if (!strcmp (arg, "--to-vevent20")) {
			type = TARGET_VEVENT_20;
		} else if (!strcmp (arg, "--to-vnote11")) {
			type = TARGET_VNOTE_11;
		} else if (!strcmp (arg, "--to-vjournal")) {
			type = TARGET_VJOURNAL;
		} else if (!strcmp (arg, "--to-vtodo10")) {
			type = TARGET_VTODO_10;
		} else if (!strcmp (arg, "--to-vtodo20")) {
			type = TARGET_VTODO_20;
		} else if (!strcmp (arg, "--to-xmlformat")) {
			type = TARGET_XMLFORMAT;
		} else if (!strcmp (arg, "--out")) {
			output = argv[i + 1];
			i++;
			if (!output)
				usage (argv[0], 1);
		} else if (!strcmp (arg, "--format-config")) {
			format_config = argv[i + 1];
			i++;
			if (!format_config)
				usage (argv[0], 1);
		} else if (!strcmp (arg, "--")) {
			break;
		} else if (arg[0] == '-') {
			usage (argv[0], 1);
		} else {
			usage (argv[0], 1);
		}
	}

	format_env = osync_format_env_new(&error);
	if (!format_env) {
		fprintf(stderr, "Unable to create format environment\n");
		goto error;
	}

	if (!osync_format_env_load_plugins(format_env, NULL, &error)) {
		fprintf(stderr, "Unable to load plugins\n");
		goto error;
	}

	// read file
	if (!osync_file_read(filename, &buffer, &size, &error)) {
		fprintf(stderr, "Unable to open file\n");
		goto error;
	}
	// osync_file_read returns a \0 terminated string, but plugins
	// have to report the correct size, not strlen
	size++;


	// create OSyncData data with 'plain' as dummy objformat
	sourceformat = osync_format_env_find_objformat(format_env, "plain");
	if (!(data = osync_data_new(buffer, size, sourceformat, &error))) {
		goto error;
	}

	// detect source and target xmlformat
	sourceformat = osync_format_env_detect_objformat(format_env, data);
	if (sourceformat) {
		osync_data_set_objformat(data, sourceformat);
	}
	else {
		// if sourceformat cannot be detected, then we'll segfault
		// below, in osync_objformat_get_name(), and we can't use
		// "plain", so fail right here
		goto error;
	}

	targetformat = conv_run_detection(format_env, osync_objformat_get_name(sourceformat), type);
	if (!targetformat) {
		goto error;
	}

	// find converter path
	OSyncFormatConverterPath *path = osync_format_env_find_path(format_env, sourceformat, targetformat, &error);

	if (format_config)
		osync_converter_path_set_config(path, format_config);

	// convert data
	if (!osync_format_env_convert(format_env, path, data, &error)) {
		fprintf(stderr, "Unable to convert data\n");
		goto error;
	}

	// print data or store it into 'output'
	print = osync_data_get_printable(data, &error);
	if (!print)
		goto error;

	if (output) {
		if (!osync_file_write(output, print, strlen(print), 0644, &error)) {
			fprintf(stderr, "Unable to write file %s", output);
			goto error;
		}
	} else {
		printf("%s", print);
	}
	osync_free(print);

	osync_data_unref(data);
	osync_converter_path_unref(path);
	osync_format_env_unref(format_env);

	return 0;

error:
	if (error)
		fprintf(stderr, "%s\n", osync_error_print(&error));

	osync_error_unref(&error);
	return 1;
}
