/*
 * xmlformat-common - common code for all xmlformat converter 
 * Copyright (C) 2004-2005  Armin Bauer <armin.bauer@opensync.org>
 * Copyright (C) 2006  Daniel Friedrich <daniel.friedrich@opensync.org>
 * Copyright (C) 2007  Daniel Gollub <gollub@b1-systems.de>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
 * 
 */

#ifndef XMLFORMAT_COMMON_H_
#define XMLFORMAT_COMMON_H_

#include <stdio.h>
#include <string.h>

#include <opensync/opensync.h>
#include <opensync/opensync-xmlformat.h>
#include <opensync/opensync-format.h>
#include <opensync/opensync-time.h>
#include <opensync/opensync-capabilities.h>

#include "vformat.h"

#define HANDLE_IGNORE (void *)1

typedef struct OSyncHookTables {
	GHashTable *attributes;
	GHashTable *parameters;
	GHashTable *tztable; // hashtable for VTIMEZONE handler
	GHashTable *alarmtable; // hashtable for VALARM handler
} OSyncHookTables;

// Handler functions pointer
typedef void (* param_handler_fn) (OSyncXMLField *xmlfield, VFormatParam *param);
typedef OSyncXMLField * (* attr_handler_fn) (OSyncXMLFormat *xmlformat, VFormatAttribute *attr, OSyncError **error);
typedef void (* attr_component_handler_fn) (OSyncXMLField *xmlfield, VFormatAttribute *attr);

/** VFormat Attributes **/
OSyncXMLField *handle_attribute_simple_content_timestamp(OSyncXMLFormat *xmlformat, VFormatAttribute *attr, const char *name, OSyncError **error); 
OSyncXMLField *handle_attribute_simple_content(OSyncXMLFormat *xmlformat, VFormatAttribute *attr, const char *name, OSyncError **error); 
OSyncXMLField *handle_categories_attribute(OSyncXMLFormat *xmlformat, VFormatAttribute *attr, OSyncError **error);
OSyncXMLField *handle_class_attribute(OSyncXMLFormat *xmlformat, VFormatAttribute *attr, OSyncError **error);
OSyncXMLField *handle_uid_attribute(OSyncXMLFormat *xmlformat, VFormatAttribute *attr, OSyncError **error); 
OSyncXMLField *handle_url_attribute(OSyncXMLFormat *xmlformat, VFormatAttribute *attr, OSyncError **error); 
// TODO: osync_bool handle_simple_xmlfield(OSyncXMLField *xmlfield, VFormatAttribute *attr, const char *name, OSyncError **error);
osync_bool handle_simple_xmlfield(OSyncXMLField *xmlfield, VFormatAttribute *attr, const char *name);

osync_bool FIXME_xmlfield_set_key_value(OSyncXMLField *xmlfield, const char *key, const char *value);

/** XML Attributes **/
VFormatAttribute *handle_xml_attribute_simple_content(VFormat *vformat, OSyncXMLField *xmlfield, const char *name, const char *encoding);
VFormatAttribute *handle_xml_attribute_simple_content_timestamp(VFormat *vformat, OSyncXMLField *xmlfield, const char *name, const char *encoding);
VFormatAttribute *handle_xml_categories_attribute(VFormat *vcard, OSyncXMLField *xmlfield, const char *encoding);
VFormatAttribute *handle_xml_class_attribute(VFormat *vcard, OSyncXMLField *xmlfield, const char *encoding);
VFormatAttribute *handle_xml_uid_attribute(VFormat *vcard, OSyncXMLField *xmlfield, const char *encoding);
VFormatAttribute *handle_xml_url_attribute(VFormat *vcard, OSyncXMLField *xmlfield, const char *encoding);

osync_bool needs_encoding(const unsigned char *tmp, const char *encoding);
osync_bool needs_charset(const unsigned char *tmp);

void add_value(VFormatAttribute *attr, OSyncXMLField *xmlfield, const char *name, const char *encoding);
void add_value_data(VFormatAttribute *attr, OSyncXMLField *xmlfield, const char *data, const char *encoding);
void add_value_array(VFormatAttribute *attr, OSyncXMLField *xmlfield, const char *parameterNameArray[], int nParameters, const char *encoding);
void add_values(VFormatAttribute *attr, OSyncXMLField *xmlfield, const char *encoding);
void add_values_from_nth_field_on(VFormatAttribute *attr, OSyncXMLField *xmlfield, const char *encoding, int nth);

/* VFormat Handler for Attributes and Parameters */
void handle_parameter(GHashTable *paramtable, OSyncXMLField *xmlfield, VFormatParam *param);
void handle_attribute(GHashTable *attrtable, GHashTable *paramtable, OSyncXMLFormat *xmlformat, VFormatAttribute *attr, OSyncError **error);
void handle_component_attribute(GHashTable *attrtable, GHashTable *paramtable, OSyncXMLField *xmlfield, VFormatAttribute *attr, OSyncError **error);

/* XML Handler for Attributes and Parameters */
void xml_handle_parameter(OSyncHookTables *hooks, VFormatAttribute *attr, OSyncXMLField *xmlfield, int attr_nr);
void xml_handle_attribute(OSyncHookTables *hooks, VFormat *vcard, OSyncXMLField *xmlfield, const char *encoding);
void xml_handle_component_attribute(OSyncHookTables *hooks, VFormat *vcard, OSyncXMLField *xmlfield, const char *encoding);

/* Helper function for hash table usage */
void insert_xml_attr_handler(GHashTable *table, const char *name, void *handler);
void insert_param_handler(GHashTable *table, const char *paramname, param_handler_fn handler);
void insert_attr_handler(GHashTable *table, const char *attrname, attr_handler_fn handler);
void insert_attr_component_handler(GHashTable *table, const char *attrname, attr_component_handler_fn handler);

#endif // XMLFORMAT_COMMON_H_

